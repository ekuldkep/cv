﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

    private Rigidbody2D _characterRigidBody;
    [SerializeField]
    private float _movementSpeed;

    private bool _facingRight;
    private Animator _animator;
    [SerializeField]
    private float JumpSpeed;

    void Start()
    {
        _facingRight = true;
        _characterRigidBody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        GameObject.Find("jwg2").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("itk2").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("twister2").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("interests2").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("tlu").transform.localScale = new Vector3(0, 0, 0);
        



    }



    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        HandleMovements(moveHorizontal);
        Flip(moveHorizontal);

        if (Input.GetKeyDown("space"))
            Jump(moveHorizontal);


    }
    void Jump(float moveHorizontal)
    {

        _characterRigidBody.velocity = new Vector2(moveHorizontal * _movementSpeed, _characterRigidBody.velocity.y) + Vector2.up * JumpSpeed;
    }

    private void HandleMovements(float moveHorizontal)
    {

        _characterRigidBody.velocity = new Vector2(moveHorizontal * _movementSpeed, _characterRigidBody.velocity.y);
        _animator.SetFloat("speed", Mathf.Abs(moveHorizontal));

    }



    private void Flip(float moveHorizontal)
    {
        if (moveHorizontal > 0 && !_facingRight || moveHorizontal < 0 && _facingRight)
        {
            _facingRight = !_facingRight;
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log(col.gameObject.name);
        if (col.gameObject.name == "school")
        {
            GameObject.Find("jwg2").transform.localScale = new Vector3(2, 3, 2);
        }
        if (col.gameObject.name == "it")
        {
            GameObject.Find("itk2").transform.localScale = new Vector3(2, 3, 2);
        }
        if (col.gameObject.name == "trampoline")
        {
            GameObject.Find("twister2").transform.localScale = new Vector3(2, 3, 2);
        }
        if (col.gameObject.name == "book")
        {
            GameObject.Find("interests2").transform.localScale = new Vector3(2, 3, 2);
        }
        if (col.gameObject.name == "beebi")
        {
            GameObject.Find("tlu").transform.localScale = new Vector3(2, 3, 2);
        }

    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.name == "school")
        {
            GameObject.Find("jwg2").transform.localScale = new Vector3(0, 0, 0);
        }
        if (col.gameObject.name == "it")
        {
            GameObject.Find("itk2").transform.localScale = new Vector3(0, 0, 0);
        }
        if (col.gameObject.name == "trampoline")
        {
            GameObject.Find("twister2").transform.localScale = new Vector3(0, 0, 0);
        }
        if (col.gameObject.name == "book")
        {
            GameObject.Find("interests2").transform.localScale = new Vector3(0, 0, 0);
        }
        if (col.gameObject.name == "beebi")
        {
            GameObject.Find("tlu").transform.localScale = new Vector3(0, 0, 0);
        }

    }
}
